package main

import "fmt"

func main() {

	// В качестве ключа можно использовать любой тип данных для которого определена операция сравнения
	// m := map[func(int){}]stuct{}{} - error
	// m := map[[]int]struct{}{} - error
	// m := map[map[string]string]struct{}{} - error

	// статические массивы можно
	m := map[[4]int]string{}
	m[[4]int{1, 2, 3, 4}] = "1234"
	m[[4]int{1, 1, 3, 4}] = "1134"
	fmt.Println(m[[4]int{1, 1, 3, 4}])

	// каналы можно
	chanM := map[chan int]string{} // - not error
	c1 := make(chan int, 1)
	c2 := make(chan int, 1)
	chanM[c1] = "c1"
	chanM[c2] = "c2"
	fmt.Println(chanM[c2])

	// например хотим хранить матрицу массив
	type key struct {
		i, j int
	}
	matrix := map[key]int{}
	matrix[key{i: 1, j: 3}] = 2

	/*
		type key2 struct {
			s []int
			i, j int
		}
		matrix2 := map[key2]int{} - error
	*/

	/*
		type key3 struct {
			m    map[string]string
			i, j int
		}
		matrix3 := map[key3]int{} - error
	*/
}
