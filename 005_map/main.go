package main

import "fmt"

func main() {
	var m map[string]int
	m = map[string]int{
		"USA":    331900000,
		"China":  1412000000,
		"Russia": 143400000,
	}
	fmt.Println(m["USA"])
	for key, val := range m {
		fmt.Println("key: ", key, "val: ", val)
	}
	delete(m, "USA")
	fmt.Println(m)

	if _, ok := m["India"]; !ok {
		m["India"] = 1408000000
	}

	m["China"] = 1412000001
	fmt.Println(m)
}
