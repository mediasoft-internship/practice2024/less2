package main

import "fmt"

func main() {
	/*
		https://go.dev/src/runtime/slice.go

		type slice struct {
			array unsafe.Pointer
			len   int
			cap   int
		}
	*/
	var arr1 []int
	fmt.Println(arr1, len(arr1), cap(arr1))

	arr2 := []int{1, 1, 1, 1}
	fmt.Println(arr2, len(arr2), cap(arr2))

	arr3 := make([]int, 4)
	fmt.Println(arr3, len(arr3), cap(arr3))

	arr4 := make([]int, 0, 4)
	fmt.Println(arr4, len(arr4), cap(arr4))

	arr3[0] = 1
	arr3[1] = 2
	arr3[2] = 3
	arr3[3] = 4
	fmt.Println(arr3, len(arr3), cap(arr3))

	// APPEND

	//arr4[0] = 1
	//arr4[1] = 2
	//arr4[2] = 3
	//arr4[3] = 4
	//fmt.Println(arr4, len(arr4), cap(arr4))
	arr4 = append(arr4, 1, 2, 3, 4, 5, 6)
	fmt.Println(arr4, len(arr4), cap(arr4))

	// CAPACITY

	for i := 0; i < 20; i++ {
		arr1 = append(arr1, i)
		fmt.Println("cap(arr1): ", cap(arr1))
	}

	size := 257
	arr5 := make([]int, size)
	arr5 = append(arr5, arr1...)
	fmt.Println("delta cap(arr5)", cap(arr5)-size)

	// SLICE

	arr2 = arr1[:10]
	fmt.Println("arr1[:10] = ", arr2)
	fmt.Println("arr1[5:10] = ", arr1[5:10])
	fmt.Println("arr1[10:] = ", arr1[10:])

	arr2[2] = 100
	arr2[3] = 111
	arr2[4] = 111

	fmt.Println("arr1 = ", arr1, "\narr2 = ", arr2)

	fmt.Println("cap(arr2) = ", cap(arr2), " len(arr2) = ", len(arr2))
	arr2 = append(arr2, 333, 444, 555, 666, 777, 888, 9999)

	fmt.Println("arr1 = ", arr1, "\narr2 = ", arr2)

	arr4 = arr1[18:]
	arr4 = append(arr4, 1, 3, 4, 7, 8)
	a := make([]int, 100)
	arr4 = append(arr4, a...)
	a[0] = 1000
	arr1[0] = 999

	fmt.Println("arr1 = ", arr1, "\narr4 = ", arr4)

}
