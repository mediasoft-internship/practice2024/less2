# less2


 __Домашнее задание__

+ Написать три структуры данных на выбор (примеры заготовок кода ниже):
    + Стек
    + Очередь 
    + Односвязанный список 
    + Двусвязанный список 
    + Бинарное дерево

+ Сделать конвертер из римских цифр в арабские 
+ Заполнить двумерный массив случайными уникальными числами


### Стек

```go

package main

type stack struct {
	s []any // слайс в котором хранятся значения в стеке
	head int // индекс головы стека
}

func newStack(size int) *stack {
	return &stack{
		s: make([]any, size),
		head: -1,
    }
}

// push - добавление в стек значения
func push(s *stack, v any) {
	// ...	
}

// pop - получения значения из стека и его удаление из вершины
func pop(s *stack) any {
	// ...
}

// peek - просмотр значения на вершине стека
func peek(s *stack) any {
	// ...
}

```


### Очередь

```go
package main

type queue struct {
	s []any // слайс в котором хранятся значения
	low, high int // индексы верхней и нижней границы очереди
	size int // размер очереди
}

func newQueue(size ) *queue {
	return &queue{
		s: make([]any, size),
		size: size,
		low: -1,
		high: -1,
    }
}

// push - добавление в очередь значения
func push(q *queue, v any) {
  // ...	
}

// pop - получения значения из очереди и его удаление
func pop(q *queue, v any) {
	// ...	
}

```

### Односвязанный список

```go
package main

type singlyLinkedList struct {
  first *item
  last  *item
  size  int
}

type item struct {
	v any
	next *item
}

func newSinglyLinkedList() *singlyLinkedList {
  return singlyLinkedList{}
}


// add - добавление значения в связный список
func add(l *singlyLinkedList, v any) {
  // ...
}

// get - получение значения по индексу из связанного списка
func get(l *singlyLinkedList, idx int) any {
  // ...
}

// remove - удаление значения по индексу из списка
func remove(l *singlyLinkedList, idx int) {
  // ...
}

// values - получение слайса значений из списка
func values(l *singlyLinkedList) []any {
  // ...
}

```

### Двусвязанный список

```go
package main

type doublyLinkedList struct {
  first *item
  last  *item
  size  int
}

type item struct {
  v any
  next *item
  prev  *item
}

func newDoublyLinkedList() *doublyLinkedList {
  return doublyLinkedList{}
}

// add - добавление значения в связный список
func add(l *doublyLinkedList, v any) {
  // ...
}

// get - получение значения по индексу из связанного списка
func get(l *doublyLinkedList, idx int) any {
  // ...
}

// remove - удаление значения по индексу из списка
func remove(l *doublyLinkedList, idx int) {
  // ...
}

// values - получение слайса значений из списка
func values(l *doublyLinkedList) []any {
  // ...
}

```


### Бинарное дерево

```go
package main

type tree struct {
	head *node
}

type node struct {
	left, right *node
	v int
}

func newTree() *tree {
  return &tree{}
}

// add - добавление значения в дерево
func add(t *tree, v int) {
}

// remove - удаление значения из дерева
func remove(l *tree, v int) {
  // ...
}

// values - получение отсортированного слайса значений из дерева
func values(l *tree) []int {
  // ...
}

```

