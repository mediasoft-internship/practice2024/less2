package main

import (
	"errors"
	"fmt"
	"io"
	"log"
	"os"
)

func readFile(path string) string {
	file, err := os.OpenFile(path, os.O_RDONLY, 0666)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err != file.Close() {
			log.Printf("error when closion file: %v", err)
		}
	}()

	text := ""
	b := make([]byte, 64)
	for {
		n, err := file.Read(b)
		if errors.Is(err, io.EOF) {
			break
		} else if err != nil {

			log.Fatal(err)
		}

		text += string(b[:n])
	}

	return text
}

func main() {
	fmt.Println(readFile("./009-defer/text.txt"))
}
