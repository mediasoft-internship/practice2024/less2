package main

import "fmt"

func main() {
	m, n := 5, 10
	matrix := make([][]int, m)
	for i := range matrix {
		matrix[i] = make([]int, n)
	}

	for i := range matrix {
		for j := range matrix[i] {
			matrix[i][j] = i + j
		}
	}

	for _, row := range matrix {
		for _, value := range row {
			fmt.Print(value, "\t")
		}
		fmt.Println()
	}
}
