package main

import "fmt"

func main() {
	fmt.Println(parity(2))
	fmt.Println(parity(3))
	fmt.Println(c())
}

func parity(n int) (s string) {
	defer func() {
		s += " number"
	}()
	if n%2 == 0 {
		s = "even"
		return
	} else {
		// s = "odd"
		return "odd"
	}
}

func c() (i int) {
	defer func() { i++ }()
	return 9
}
