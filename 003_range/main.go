package main

import "fmt"

func main() {
	type Human struct {
		ID   int
		Name string
	}

	s := []*Human{{Name: "Gopher"}, {Name: "Rick"}}
	for i, h := range s {
		h.ID = i
	}

	for _, h := range s {
		fmt.Printf("ID: %d, Name: %s\n", h.ID, h.Name)
	}

}
