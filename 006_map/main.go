package main

import "fmt"

func main() {
	nums := map[rune]int{
		'0': 0,
		'1': 1,
		'2': 2,
		'3': 3,
		'4': 4,
		'5': 5,
		'6': 6,
		'7': 7,
		'8': 8,
		'9': 9,
	}

	str := "100223"
	out := 0
	r := []rune(str)

	n := 1
	for i := len(r) - 1; i >= 0; i-- {
		out += nums[r[i]] * n
		n *= 10
	}
	fmt.Println(out)
}
