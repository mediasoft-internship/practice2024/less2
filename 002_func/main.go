package main

import "fmt"

func main() {
	// Передача слайса в функции
	//a := make([]int, 0, 200)
	//a = append(a, 1, 2, 3, 4)
	a := []int{1, 2, 3, 4} // len = 4, cap = 4
	a = addItems(a, 20)
	fmt.Println(a)

	squareSlice(a)
	fmt.Println(a)
}

func addItems(a []int, count int) []int {
	for i := 0; i < count; i++ {
		a = append(a, i)
	}

	return a
}

func squareSlice(a []int) {
	for i := range a {
		a[i] *= a[i]
	}
}
